<?php

/**
 * @file
 * Implements PayTrek payment services for use with Drupal Commerce.
 */

/**
 * Defines test payment mode for PayTrek.
 */
define('COMMERCE_PAYTREK_SANDBOX', 'sandbox');

/**
 * Defines live payment mode for PayTrek.
 */
define('COMMERCE_PAYTREK_LIVE', 'secure');

/**
 * Defines for endpoints of PayTrek API.
 */
define('COMMERCE_PAYTREK_SALE_URL', '/api/v1/sale/');
define('COMMERCE_PAYTREK_CHARGE_URL', '/api/v1/charge/');


/**
 * Creating sale entity and send to PayTrek.
 *
 * @param $payment_method
 *   The payment method instance array with settings.
 * @param $order
 *   The order object the payment applies to.
 * @param $charge
 *   An array indicating the amount and currency code to charge.
 * @return object
 *   An object with status code and response from PayTrek.
 */
function commerce_paytrek_create_sale($payment_method, $order, $charge) {
  $order_wrapper = entity_metadata_wrapper($order->type, $order);

  $line_items = $order_wrapper->commerce_line_items->value();
  $billing = $order_wrapper->commerce_customer_billing->value();
  // @todo need recheck this.
  $customer_billing = $billing->commerce_customer_address[LANGUAGE_NONE][0];

  // Build array with items for PayTrek.
  foreach ($line_items as $item) {
    $sale_items[] = array(
      'name' => $item->line_item_label,
      'quantity' => round($item->quantity),
      'unit_price' => $item->commerce_unit_price[LANGUAGE_NONE][0]['amount'] / 100,
    );
  }

  $sale_body = array(
    'order_id' => $order->order_id,
    'secure_option' => 'No',
    'installment' => 1,
    'amount' => $charge['amount'] / 100,
    'currency' => '/api/v1/currency/' . $charge['currency_code'] . '/',
    'customer_first_name' => $customer_billing['first_name'],
    'customer_last_name' => $customer_billing['last_name'],
    'customer_email' => $order->mail,
    'billing_country' => $customer_billing['country'],
    'billing_state' => isset($customer_billing['administrative_area']) ? $customer_billing['administrative_area'] : '',
    'billing_city' => $customer_billing['locality'],
    'billing_zipcode' => $customer_billing['postal_code'],
    'billing_address' => $customer_billing['thoroughfare'],
    'items' => $sale_items,
    'customer_ip_address' => $order->hostname,
  );

  // Make a request to the service.
  return _commerce_paytrek_api_request(COMMERCE_PAYTREK_SALE_URL, $payment_method['settings'], $sale_body);
}

/**
 * Request to the PayTrek API.
 *
 * @param $settings
 *   The array with setting for the request.
 * @param $request_body
 *   The array body for the request, sale data, charge data etc.
 * @return object
 *   An object with status code and response from PayTrek.
 */
function _commerce_paytrek_api_request($endpoint, $settings, $request_body) {
  $ch = curl_init();

  switch ($settings['mode']) {
    case COMMERCE_PAYTREK_SANDBOX:
      $api_url = 'https://sandbox.paytrek.com/' . $endpoint;
      break;

    case COMMERCE_PAYTREK_LIVE:
      $api_url = 'https://secure.paytrek.com/' . $endpoint;
      break;
  }

  curl_setopt($ch, CURLOPT_URL, $api_url);
  curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'POST');
  curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
  curl_setopt($ch, CURLOPT_USERPWD, $settings['api_username'] . ':' . $settings['api_password']);
  curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
  curl_setopt($ch, CURLOPT_POST, 1);
  curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($request_body));
  curl_setopt($ch, CURLOPT_HTTPHEADER, array(
    'Content-Language: en-us',
    'Content-Type: application/json',
  ));

  $response = curl_exec($ch);
  $response_code = curl_getinfo($ch, CURLINFO_HTTP_CODE);
  curl_close($ch);

  return (object) array(
    'response' => $response ? json_decode($response) : FALSE,
    'status_code' => $response_code
  );
}
